%define soname libgmenuharness

Name:		gmenuharness
Version:	0.1.2
Release:	1%{?dist}
Summary:	Testing enablement library for projects exporting GiO's gmenu/gaction endpoints

Group:		Libraries
License:	Custom
URL:		https://launchpad.net/gmenuharness
Source0:	https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/%{name}/0.1.2+17.04.20161202-0ubuntu1/%{name}_%{version}+17.04.20161202.orig.tar.gz
Patch1:		Functional1.patch
Patch2:		Functional2.patch

BuildRequires:	cmake, cmake-extras, doxygen, gtest-devel
Requires:	unity-api, gtest, libqtdbustest

##Rest of the stuff is detected by

BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(libunity-api)
BuildRequires:	pkgconfig(libqtdbustest-1)

%description
Testing enablement library for projects exporting GiO's gmenu/gaction endpoints

%package -n %{soname}
Summary:	Testing enablement library for projects exporting GiO's gmenu/gaction endpoints -- Libraries
Group:		System/Libraries

%description -n %{soname}
Testing enablement library for projects exporting GiO's gmenu/gaction endpoints.
This package provides libraries for %{name} package


%package devel
Summary:	Testing enablement library for projects exporting GiO's gmenu/gaction endpoints -- Development files
Group:		Development/Libraries

%description devel
Testing enablement library for projects exporting GiO's gmenu/gaction endpoints.
This package provides Development libraries for %{name} package.

%prep
%setup -q -n %{name}_%{version}+17.04.20161202.orig
%patch1 -p1
%patch2 -p1

%build
%cmake  \
	-DCMAKE_INSTALL_PREFIX:PATH=%{_prefix}  \
	-DCMAKE_INSTALL_LIBDIR=%{_libdir}
make %{?_smp_mflags}




%check
make check

%install
%make_install

%clean
rm -rf $RPM_BUILD_ROOT

%post -n %{soname} -p /sbin/ldconfig
%postun -n %{soname} -p /sbin/ldconfig


%files devel
%license COPYING
%doc README INSTALL
%{_includedir}/%{name}-0.1/unity/%{name}/*.h
%{_libdir}/pkgconfig/libgmenuharness.pc

%files -n %{soname}
%{_libdir}/libgmenuharness.so
%{_libdir}/libgmenuharness.so.0.1
%{_libdir}/libgmenuharness.so.0.1.0


%changelog
* Mon Oct 1 2018 Abhishek Mudgal <abhishek.mudgal.59@gmail.com>
- Create Spec file


